const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Product = require('./ProductModel')
const NoteSchema = Schema(
    {
        productId: { type: mongoose.Schema.Types.ObjectId, ref: Product },
        amount: Number,
    }
);

const Booking = mongoose.model("booking", NoteSchema);

module.exports = Booking;