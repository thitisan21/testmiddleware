const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userScema = Schema(
    {
        username: String,
        password: String,
        role: String
    }
);

module.exports = mongoose.model("user", userScema);;