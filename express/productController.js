const Product = require('./ProductModel');

module.exports.findAll = (req, res) => {
    Product.find({}).sort({ amount: 1 }).exec(function (err, obj) {
        let array = [];
        if (!err) {
            obj.forEach(element => {
                if (element.status) {
                    array.push(element)
                }
            });
            res.json(array)
        }
    })
}