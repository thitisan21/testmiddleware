
module.exports = (app) => {
    const glassRequestController = require('./productController');
    const AuthController = require('./auth')

    app.get('/product/findAll', AuthController.roleAuthorization(['ผู้ดูแลระบบ']), glassRequestController.findAll);
    //app.post('/glass-request/create', AuthController.roleAuthorization(['อาจารย์', 'เจ้าหน้าที่', 'นิสิต','เจ้าหน้าที่สารสนเทศ','เจ้าหน้าที่บุคคล','บุคลากร', 'หัวหน้าบุคลากร'  ]), glassRequestController.create);

}
