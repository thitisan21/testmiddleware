const userModels = require('./user');

module.exports.findPerson = (req, res) => {
    userModels.find({}).sort({ name: 1 }).exec((err, obj) => {
        if (!err) {
            res.json(obj)
        } else {
            res.status(500).json({ status: false })
        }
    })
}

// exports.create = (req, res) => {
//     userModels.findOne({ username: req.body.username }, (_, result) => {
//         if (result === null) {
//             userModels.create(req.body, (err, result) => { })
//             res.json({ status: true })
//         } else {
//             res.json({ status: false })
//         }
//     });
// }

