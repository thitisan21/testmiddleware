const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const NoteSchema = Schema(
    {
        name: String,
        style: String,
        color: String,
        model: String,
        amount: Number,
        price: Number,
        status: Boolean,
        date: Date,
        cost: Number,
        allCost: Number,
        sell: Number,
        profit: Number,
        totalBuy: Number,
        discount: Number,
        persen: Number,
        img: String,
        barcode: String
    }
);

const Product = mongoose.model("product", NoteSchema);

module.exports = Product;