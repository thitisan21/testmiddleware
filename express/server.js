'use strict';

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const serverless = require('serverless-http');

//models
const Product = require('./ProductModel');
const Orther = require('./orther');
const Booking = require('./booking');
const router = express.Router();

const AuthController = require('./auth')
// mongoose.connect(
//   'mongodb+srv://admin:j4GXgToVUJxrQUhG@cluster0-k2drd.mongodb.net/product?retryWrites=true&w=majority',
//   { useNewUrlParser: true }
// );
mongoose.connect(
  'mongodb+srv://admin:j4GXgToVUJxrQUhG@cluster0-k2drd.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true }
);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

router.get('/api/v1/products', AuthController.roleAuthorization(['ผู้ดูแลระบบ']), (req, res) => {
  Product.find({}).sort({ amount: 1 }).exec(function (err, obj) {
    let array = [];
    if (!err) {
      obj.forEach(element => {
        if (element.status) {
          array.push(element)
        }
      });
      res.json(array)
    }
  })
});

router.get('/api/v1/products/Asc', (req, res) => {
  Product.find({}).sort({ amount: -1 }).exec(function (err, obj) {
    let array = [];
    if (!err) {
      obj.forEach(element => {
        if (element.status) {
          array.push(element)
        }
      });
      res.json(array)
    }
  })
});

router.get('/api/v1/products/sortname', (req, res) => {
  Product.find({}).sort({ name: 1 }).exec(function (err, obj) {
    let array = [];
    if (!err) {
      obj.forEach(element => {
        if (element.status) {
          array.push(element)
        }
      });
      res.json(array)
    }
  })
});

router.get('/api/v1/products/sortcome', (req, res) => {
  Product.find({}).sort({ profit: -1 }).exec(function (err, obj) {
    let array = [];
    if (!err) {
      obj.forEach(element => {
        if (element.status) {
          array.push(element)
        }
      });
      res.json(array)
    }
  })
});



router.get('/api/v1/products/cal', async (req, res) => {
  await Product.find({}, function (err, obj) {
    let array = obj;
    var inSell;
    // if (!err) {
    array.forEach(item => {
      inSell = item.price * item.sell;
      console.log(inSell)
    })
    //   res.json(array)
    // }
    // res.json(obj)
  })
});

router.get('/api/v1/products/delete', (req, res) => {
  Product.find({}, function (err, obj) {
    let array = [];
    if (!err) {
      obj.forEach(element => {
        if (!element.status) {
          array.push(element)
        }
      });
      res.json(array)
    }
  })
});

router.get('/api/v1/products/:id', (req, res) => {
  Product.findOne({ _id: req.params.id }, function (err, obj) {
    if (!err) {
      res.json(obj)
    }
  })
});

router.get('/api/v1/pro', (req, res) => {
  Product.find({}, function (err, obj) {
    let array = 0;
    let cost = 0;
    if (!err) {
      obj.forEach(element => {
        if (element.profit) {
          array = array + element.profit;
        }
        if (element.allCost) {
          cost = cost + element.allCost
        }
      });
      res.json({ profit: array, allCost: cost })
    }
  })
});

router.post('/api/v1/products', (req, res) => {
  Product.create(req.body, (err) => {
    res.json({ status: true })
  });
});

router.post('/api/v1/orther/add', (req, res) => {
  Orther.create(req.body, (err) => {
    res.json({ status: true })
  })
});

router.get('/api/v1/orther/get', (req, res) => {
  Orther.find({}, (err, obj) => {
    let array = 0;
    if (!err) {
      obj.forEach(element => {
        if (element.name) {
          array = array + element.price;
        }
      });
      res.json(array)
    }
  })
});


router.put('/api/v1/products/:id', (req, res) => {
  Product.findByIdAndUpdate(req.params.id, req.body, (err, Product) => {
    if (err) return res.status(404).send({ message: err.message });
    return res.send({ message: 'Product updated!', Product });
  });
});

router.put('/api/v1/booking/:id', (req, res) => {
  const amount = {
    amount: req.body.amountAll
  }
  Product.findByIdAndUpdate(req.params.id, amount).exec(function (err, obj) {
    if (err) {
      res.status(404)
    } else {
      const amount = {
        amount: req.body.amount,
        productId: req.params.id
      }
      Booking.find({ productId: req.body.productId }, (err, obj2) => {
        if (obj2.length === 0) {
          Booking.create(amount, (err) => {
            if (!err) {
              res.json({ status: true })
            }
          })
        } else {
          const amount2 = {
            amount: req.body.amount + 1,
          }
          const _id = obj2[0]._id;
          Booking.findByIdAndUpdate(_id, amount2, (hasError, obj3) => {
            if (hasError) return res.status(404).send({ message: hasError.message });
            return res.send({ message: 'Product updated!', obj3 });
          })
        }
      })
      // if (obj2.length === 0) {
      //   Booking.create(amount, (err) => {
      //     if (!err) {
      //       res.json({ status: true })
      //     }
      //   })
      // } else {
      //   const amount2 = {
      //     amount: req.body.amount + 1,
      //   }
      //   const _id = obj2[0]._id;
      //   Booking.findByIdAndUpdate(_id, amount2, (hasError, obj3) => {
      //     if (hasError) return res.status(404).send({ message: hasError.message });
      //     return res.send({ message: 'Product updated!', obj3 });
      //   })
      // }
      //})
    }
  })
})

router.post('/api/v1/barcode', (req, res) => {
  Product.findOne({ barcode: req.body.barcode }).exec(async function (err, obj) {
    if (obj !== null) {
      const amount = obj.amount;
      const afterAmount = obj.amount - 1;
      const afterSell = obj.sell + 1;
      const price = obj.price;
      const sell = obj.sell;
      const cost = obj.cost;
      if (amount > 0) {
        const setValue = {
          amount: afterAmount,
          sell: afterSell,
          allCost: (afterAmount + afterSell) * cost,
          profit: price * afterSell,
        }
        await Product.update({ _id: obj._id }, setValue, (err, Product) => {
          if (err) return res.status(404).send({ message: err.message });
          return res.json({ status: true });
        });
      } else {
        res.json({ status: false });
      }
    } else {
      res.json({ status: false });
    }
  })
});

router.put('/api/v1/products/sell/:id', (req, res) => {
  // Product.findOne({ _id: req.params.id }, (err, obj) => {
  //   amountSell = req.body.sell;
  //   if (!err) {
  //     res.json(obj)
  //   }
  // }).exec(function (err, post) {
  //   post.sell = post.sell + amountSell
  //   // console.log(post);
  //   Product.findByIdAndUpdate(req.params.id, post, (err, Product) => {
  //     if (err) return res.status(404).send({ message: err.message });
  //     return res.send({ message: 'Product updated!', Product });
  //   });
  // })
  console.log(req.body);
  Product.findByIdAndUpdate(req.params.id, req.body, (err, Product) => {
    if (err) return res.status(404).send({ message: err.message });
    return res.send({ message: 'Product updated!', Product });
  });
});

router.delete('/api/v1/products/:id', (req, res) => {
  // Product.findByIdAndRemove(req.params.id, (err) => {
  //   if (err) return res.status(404).send({ message: err.message });
  //   return res.send({ message: 'Product deleted!' });
  // });
});

router.get('/api/v1/booking/get', (req, res) => {
  Booking.find({}).populate('productId').exec((err, obj) => {
    res.json(obj)
  })
});

router.post('/api/v1/products/barcode', (req, res) => {
  Product.findOne({ barcode: req.body.barcode }).exec(async function (err, obj) {
    res.json(obj);
  })
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use('/.netlify/functions/server', router);
require('./loginRoutes')(app)
require('./productRoutes')(app)

module.exports = app;
module.exports.handler = serverless(app);