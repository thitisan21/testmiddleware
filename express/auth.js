'use strict';
//const jwt = require('jsonwebtoken')
const User = require('./user')
// const UserAccess = require('../models/userAccess')
// const ForgotPassword = require('../models/forgotPassword')
const utils = require('./utils')
//const key = require('../../config/keys')
// const uuid = require('uuid')
// const { addHours } = require('date-fns')
// const { matchedData } = require('express-validator/filter')
// const auth = require('../middleware/auth')
// const emailer = require('../middleware/emailer')
// const HOURS_TO_BLOCK = 2
// const LOGIN_ATTEMPTS = 5

/*********************
 * Private functions *
 *********************/


const checkPermissions = async (data, next) => {
    return new Promise((resolve, reject) => {
        User.find({ username: data.username }, (err, result) => {
            if (result.length > 0) {
                utils.itemNotFound(err, result, reject, 'NOT_FOUND')
                for (let item of result) {
                    if (data.roles.indexOf(item.role) > -1) {
                        return resolve(next())
                    } else {
                        return reject(utils.buildErrObject(401, 'UNAUTHORIZED'))
                    }
                }
            } else {
                return reject(utils.buildErrObject(401, 'UNAUTHORIZED'))
            }
        })
    })
}


/**
 * Roles authorization function called by route
 * @param {Array} roles - roles specified on the route
 */
exports.roleAuthorization = roles => async (req, res, next) => {
    try {
        if (req.body.username !== undefined) {
            const dataBody = {
                username: req.body.username,
                roles
            }
            await checkPermissions(dataBody, next)
        } else {
            const dataParams = {
                username: req.params.id,
                roles
            }
            await checkPermissions(dataParams, next)
        }
    } catch (error) {
        utils.handleError(res, error)
    }
}